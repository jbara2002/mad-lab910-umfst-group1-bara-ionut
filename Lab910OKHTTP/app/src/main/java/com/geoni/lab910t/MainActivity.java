package com.geoni.lab910t;
import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private final OkHttpClient client = new OkHttpClient();
    private TextView responseText;

    @SuppressLint({"StaticFieldLeak", "MissingInflatedId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        responseText = findViewById(R.id.response_text);

        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {
                String url = "https://reqres.in/api/users?page=2";
                Request request = new Request.Builder()
                        .url(url)
                        .build();

                try {
                    Response response = client.newCall(request).execute();
                    return Objects.requireNonNull(response.body()).string();
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @SuppressLint("SetTextI18n")
            @Override
            protected void onPostExecute(String responseBody) {
                if (responseBody != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(responseBody);
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            sb.append("ID: ").append(dataObject.getInt("id")).append("\n");
                            sb.append("First Name: ").append(dataObject.getString("first_name")).append("\n");
                            sb.append("Last Name: ").append(dataObject.getString("last_name")).append("\n");
                            sb.append("Email: ").append(dataObject.getString("email")).append("\n");
                            sb.append("Avatar: ").append(dataObject.getString("avatar")).append("\n\n");
                        }
                        responseText.setText(sb.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        responseText.setText("Error parsing JSON data");
                    }
                } else {
                    responseText.setText("Error fetching data");
                }
            }
        }.execute();
    }
}