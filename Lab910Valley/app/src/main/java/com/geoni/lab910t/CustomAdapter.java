package com.geoni.lab910t;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {
    private final List<ClipData.Item> items;

    public CustomAdapter(List<ClipData.Item> items) {
        this.items = items;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView avatarImage;
        public TextView nameText;
        public TextView emailText;

        public ViewHolder(View itemView) {
            super(itemView);

            avatarImage = itemView.findViewById(R.id.avatar_image);
            nameText = itemView.findViewById(R.id.name_text);
            emailText = itemView.findViewById(R.id.email_text);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // Set the views with the data from the Item object
        holder.nameText.setText(User.getFirstName() + " " + User.getLastName());
        holder.emailText.setText(User.getEmail());
    }
    @Override
    public int getItemCount() {
        return items.size();
    }}