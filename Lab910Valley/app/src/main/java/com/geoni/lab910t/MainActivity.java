package com.geoni.lab910t;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private RequestQueue requestQueue;
    private TextView responseText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        responseText = findViewById(R.id.response_text);

        requestQueue = Volley.newRequestQueue(this);

        String url = "https://reqres.in/api/users?page=2";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray dataArray = response.getJSONArray("data");
                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                sb.append("ID: ").append(dataObject.getInt("id")).append("\n");
                                sb.append("First Name: ").append(dataObject.getString("first_name")).append("\n");
                                sb.append("Last Name: ").append(dataObject.getString("last_name")).append("\n");
                                sb.append("Email: ").append(dataObject.getString("email")).append("\n");
                                sb.append("Avatar: ").append(dataObject.getString("avatar")).append("\n\n");
                            }
                            responseText.setText(sb.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                            responseText.setText("Error parsing JSON data");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        responseText.setText("Error fetching data");
                    }
                });

        requestQueue.add(jsonObjectRequest);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (requestQueue != null) {
            requestQueue.cancelAll(this);
        }
    }
}